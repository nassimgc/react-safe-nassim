import React from 'react';
import {
  Container
} from 'react-bootstrap';

// components
import Digits from '../components/digits';
import Screen from '../components/screen';
import DatasScreen from '../components/datas';
import { StateProvider } from '../providers/state-provider';

const Index = () => (
  <Container id="safe" className="mt-5">
    <h4 className="display-4 text-light">React Safe</h4>
    <StateProvider>
      <Screen />
      <Digits />
      <DatasScreen />
    </StateProvider>
  </Container>
);

export default Index;
