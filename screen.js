import React, { useContext } from 'react';
import {
  Alert
} from 'react-bootstrap';

import { store } from '../providers/state-provider';

const Screen = () => {
  const { state } = useContext(store);
  const { screenPassword, screenMessage } = state;
  const { message, variant } = screenMessage;
  // console.log(screenMessage);

  return (
    <Alert variant={variant}>
      <h1>{screenPassword}</h1>
      <hr />
      <p className="mb-0">{message}</p>
    </Alert>
  );
};

export default Screen;
