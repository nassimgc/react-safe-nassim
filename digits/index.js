import React, { useContext } from 'react';
import {
  Row,
  Col,
  Button
} from 'react-bootstrap';

import { store, actionTypes } from '../../providers/state-provider';

import './index.scss';

const RowBtn = ({ children }) => (
  <Row className="mb-3">
    {children}
  </Row>
);

const ColBtn = ({ value }) => {
  const { dispatch, state } = useContext(store);
  const { screenPassword } = state;

  const onHandleClick = (e) => {
    if (screenPassword.length === 4) {
      dispatch({
        type: actionTypes.SET_MESSAGE_TO_SCREEN,
        message: 'please use max 4 number chars'
      });
      return;
    }

    dispatch({
      type: actionTypes.SET_STRING_PASSWORD,
      valuePassword: e.target.dataset.key
    });
  };

  return (
    <Col sm={4} className="d-grid gap-2">
      <Button onClick={onHandleClick} data-key={value} variant="dark">{value}</Button>
    </Col>
  );
};

const ButtonsGenerator = () => {
  const ArrayButtons = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
    [0, 'cancel']
  ];

  return (
    <>
      {ArrayButtons.map((row, index) => (
        <RowBtn key={index}>
          {row.map((value) => (
            <ColBtn key={value} value={value} />
          ))}
        </RowBtn>
      ))}
    </>
  );
};

const Digits = () => {
  const { dispatch, state } = useContext(store);

  const {
    inputPassword, buttonMessage, currentPassword, isLogin, disableSubmit
  } = state;
  const onSubmitPassword = () => {
    if (isLogin === 0) {
      dispatch({
        type: actionTypes.ENCRYPT_DATA
      });
    }
    if (currentPassword === '') {
      dispatch({
        type: actionTypes.SET_INIT_PASSWORD,
        newPassword: inputPassword
      });
    } else if (currentPassword !== '' && isLogin === 0) {
      dispatch({
        type: actionTypes.SAFE_UNLOCK
      });
    } else {
      dispatch({
        type: actionTypes.SAFE_LOCK
      });
    }
  };
  return (
  <Row className="digits">
    <Col sm={6}>
      <Row>
        <Col className="d-grid gap-2">
          <Button onClick={onSubmitPassword} disabled={ disableSubmit } className="btn--unlock-lock" variant="dark">{buttonMessage}</Button>
        </Col>
      </Row>
    </Col>
    <Col sm={6}>
      <ButtonsGenerator />
    </Col>
  </Row>
  );
};

export default Digits;
