import React, { useContext, useRef } from 'react';
import {
  Col,
  Container,
  Card,
  Row,
  Form,
  Button
} from 'react-bootstrap';
import { store, actionTypes } from '../../providers/state-provider';

const Item = ({ item }) => (
      <Col sm={3} className="mt-3">
        <Card>
          <Card.Body>
            <Card.Title>{item}</Card.Title>
            <Card.Text></Card.Text>
          </Card.Body>
          <Card.Footer className="d-grid gap-2">
          </Card.Footer>
        </Card>
      </Col>
);

const DatasScreen = () => {
  const { dispatch, state } = useContext(store);
  const { datas, isLogin } = state;
  const datasArray = JSON.parse(datas);
  const inputRef = useRef(null);
  const addNewData = () => {
    dispatch({
      type: actionTypes.ADD_NEW_DATA,
      newData: inputRef.current.value
    });
  };
  return (
      <Container fluid>
        <Row>
          {datasArray.map((item, i) => <Item key={i} item={item} />)}
        </Row>
        <Row>
        {isLogin ? <Form>
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Control type="text" name="dataField" ref={inputRef} placeholder="Input data here" />
        </Form.Group>
        <Button variant="primary" onClick={() => { addNewData(); }}>
          Add a new data
        </Button>
      </Form> : ''}
        </Row>
      </Container>
  );
};

export default DatasScreen;
