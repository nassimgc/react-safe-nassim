import React, { createContext, useReducer } from 'react';
import CryptoJS from 'crypto-js';

const initialState = {
  currentPassword: '',
  screenPassword: '',
  inputPassword: '',
  maxTries: 3,
  disableSubmit: false,
  isLogin: 0,
  isEncryptData: 1,
  buttonMessage: 'Init',
  screenMessage: {
    message: 'Init password',
    variant: 'light'
  },
  datas: JSON.stringify([])
};

const store = createContext(initialState);
const { Provider } = store;

const actionTypes = {
  SET_STRING_PASSWORD: 'SET_STRING_PASSWORD',
  SET_MESSAGE_TO_SCREEN: 'SET_MESSAGE_TO_SCREEN',
  SET_INIT_PASSWORD: 'SET_INIT_PASSWORD',
  SAFE_UNLOCK: 'SAFE_UNLOCK',
  SAFE_LOCK: 'SAFE_LOCK',
  ADD_NEW_DATA: 'ADD_NEW_DATA'
};

const StateProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    switch (action.type) {
      case actionTypes.SET_STRING_PASSWORD: {
        let { screenPassword, inputPassword } = state;

        if (action.valuePassword === 'cancel') {
          screenPassword = '';
          inputPassword = '';
        } else {
          screenPassword += action.valuePassword.replace(new RegExp('[0-9]', 'g'), '*');
          inputPassword += action.valuePassword;
        }

        return { ...state, screenPassword, inputPassword };
      }
      case actionTypes.SET_MESSAGE_TO_SCREEN: {
        const updateScreenMessage = { message: action.message, variant: 'danger' };

        return { ...state, screenMessage: updateScreenMessage };
      }
      case actionTypes.SET_INIT_PASSWORD: {
        let {
          currentPassword, screenPassword, buttonMessage, inputPassword, datas
        } = state;
        const updateScreenMessage = { message: 'Enter your code', variant: 'light' };
        currentPassword = action.newPassword;
        buttonMessage = 'Unlock';
        screenPassword = '';
        inputPassword = '';

        const dataValues = ['069804761041 1 CIC', '0996145617 2 LA BANQUE POSTALE', '08915041020 3 SOCIETE GENERALE'];
        let arrayDatas = [];
        arrayDatas = dataValues.map((data) => (
          CryptoJS.AES.encrypt(data, currentPassword).toString()
        ));
        datas = JSON.stringify(arrayDatas);
        return {
          ...state,
          screenMessage: updateScreenMessage,
          currentPassword,
          screenPassword,
          buttonMessage,
          inputPassword,
          datas
        };
      }
      case actionTypes.SAFE_UNLOCK: {
        let {
          screenPassword, buttonMessage, maxTries, isLogin, inputPassword, disableSubmit, datas
        } = state;
        const { currentPassword } = state;

        if (currentPassword === inputPassword) {
          const updateScreenMessage = { message: 'ACCESS AUTHORIZED', variant: 'success' };

          maxTries = 3;

          isLogin = 1;

          buttonMessage = 'Lock';

          screenPassword = '';

          inputPassword = '';
          let arrayDatas = [];
          arrayDatas = JSON.parse(datas).map((data) => (
            CryptoJS.AES.decrypt(data, currentPassword).toString(CryptoJS.enc.Utf8)
          ));
          datas = JSON.stringify(arrayDatas);

          return {
            ...state,
            screenMessage: updateScreenMessage,
            currentPassword,
            screenPassword,
            maxTries,
            buttonMessage,
            isLogin,
            inputPassword,
            datas
          };
        }
        const updateScreenMessage = { message: 'ACCESS DENIED', variant: 'danger' };
        maxTries -= 1;
        if (maxTries === 0) {
          disableSubmit = true;
          buttonMessage = 'Too many attempts';
        }
        screenPassword = '';
        inputPassword = '';
        return {
          ...state,
          screenMessage: updateScreenMessage,
          maxTries,
          screenPassword,
          inputPassword,
          disableSubmit,
          buttonMessage
        };
      }
      case actionTypes.SAFE_LOCK: {
        let {
          screenPassword, buttonMessage, isLogin, inputPassword, datas
        } = state;

        const { currentPassword } = state;

        const updateScreenMessage = { message: 'Enter your code', variant: 'light' };
        isLogin = 0;
        buttonMessage = 'Unlock';
        screenPassword = '';
        inputPassword = '';
        let arrayDatas = [];
        arrayDatas = JSON.parse(datas).map((data) => (
          CryptoJS.AES.encrypt(data, currentPassword).toString()
        ));
        datas = JSON.stringify(arrayDatas);
        return {
          ...state,
          screenMessage: updateScreenMessage,
          screenPassword,
          inputPassword,
          isLogin,
          buttonMessage,
          datas
        };
      }
      case actionTypes.ADD_NEW_DATA: {
        let {
          datas
        } = state;
        const arrayDatas = JSON.parse(datas);
        arrayDatas.push(action.newData);
        datas = JSON.stringify(arrayDatas);
        return {
          ...state,
          datas
        };
      }
      default:
        return state;
    }
  }, initialState);

  return <Provider value={{ state, dispatch }}>{children}</Provider>;
};

export { store, StateProvider, actionTypes };
